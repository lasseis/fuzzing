#include <stddef.h>
#include <stdint.h>
#include "main.h"

//Test push for CI

int LLVMFuzzerTestOneInput(const uint8_t * data, size_t size){
    char* ret = replace_chars((char*)data, size);
    free(ret);
    return 0;
}
